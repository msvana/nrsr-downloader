import click

import utils
from utils import download_and_create_soup


def get_meeting_day_urls(meeting):
    soup = utils.download_and_create_soup(meeting['url'])
    try:
        date_options = soup.find('select', {'id': 'SelectedDate'}).find_all('option')
        meeting_day_urls = []

        for date_option in date_options:
            date_param = date_option['value']
            meeting_day_url = f"{meeting['url']}?MeetingDate={date_param}"
            meeting_day_urls.append(meeting_day_url)
    except AttributeError:
        click.echo(f"No data found for {meeting['url']}")
        meeting_day_urls = []

    return meeting_day_urls


def get_pages(base_url):
    soup = download_and_create_soup(base_url)
    pagination = soup.find('ul', 'pagination')

    if not pagination:
        max_page = 1
    else:
        last = pagination.find('li', 'last').find('a')
        max_page = int(last['href'].split('page=')[-1]) if last else 1

    param_char = '&' if '?' in base_url else '?'
    page_urls = [f'{base_url}{param_char}page={page}' for page in range(1, max_page + 1)]
    return page_urls