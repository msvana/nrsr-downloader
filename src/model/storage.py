Q_CREATE_MEETING_TABLE = '''
    CREATE TABLE meeting (
        id INT NOT NULL PRIMARY KEY,
        election_period SMALLINT NOT NULL,
        meeting_number SMALLINT NOT NULL,
        url VARCHAR(120) NOT NULL,
        transcript_scraped TINYINT DEFAULT 0,
        videos_scraped TINYINT DEFAULT 0,
        audio_extracted TINYINT DEFAULT 0
    );
'''

Q_CREATE_SPEECH_TABLE = '''
    CREATE TABLE speech (
        id INT NOT NULL PRIMARY KEY,
        meeting_id INT NOT NULL,
        timestamp DATETIME NOT NULL,
        speaker VARCHAR(100) NOT NULL,
        speech_text TEXT NOT NULL,
        video_verified TINYINT DEFAULT 0,
        video_start INT,
        video_stop INT
    )
'''

MEETING_FLAGS = [
    'videos_scraped',
    'audio_extracted',
    'transcript_scraped'
]


def get_all_meetings(con):
    cursor = con.cursor()
    cursor.execute('SELECT * FROM meeting ORDER by election_period, meeting_number')
    meetings = cursor.fetchall()
    cursor.close()
    return meetings


def flag_meeting_as(meeting, flag, con):
    if flag in MEETING_FLAGS:
        cursor = con.cursor()
        cursor.execute(f'UPDATE meeting SET {flag} = 1 WHERE id = ?', (meeting['id'],))
        con.commit()
    else:
        raise AttributeError(f'Flag `{flag}` is not allowed')


def get_meetings_not_flagged_as(flag, con):
    if flag in MEETING_FLAGS:
        cursor = con.cursor()
        cursor.execute(f'SELECT * FROM meeting WHERE {flag} = 0 '
                       f'ORDER BY election_period, meeting_number')
        meetings = cursor.fetchall()
        cursor.close()
        return meetings
    else:
        raise AttributeError(f'Flag `{flag}` is not allowed')


def clear_meeting(meeting_id, con):
    cursor = con.cursor()
    cursor.execute('''
        UPDATE meeting 
        SET transcript_scraped = 0, videos_scraped = 0, audio_extracted = 0
        WHERE id = ?
    ''', (meeting_id,))
    cursor.execute('DELETE FROM speech WHERE meeting_id = ?', (meeting_id,))
    cursor.close()


def get_speeches_for_meeting(meeting_id, con):
    cursor = con.cursor()
    cursor.execute('''
        SELECT id, timestamp, video_start, video_stop 
        FROM speech 
        WHERE meeting_id = ? AND video_verified = 1
        ORDER BY id
    ''', (meeting_id,))
    speeches = cursor.fetchall()
    cursor.close()
    return speeches


def get_speeches_for_export(con):
    cursor = con.cursor()
    cursor.execute('''
        SELECT s.id, m.election_period, m.meeting_number, 
            s.timestamp, s.speaker, s.speech_text
        FROM meeting m JOIN speech s ON s.meeting_id = m.id 
    ''')
    speeches = cursor.fetchall()
    cursor.close()
    return speeches


def truncate_speeches(con):
    cursor = con.cursor()
    cursor.execute('UPDATE meeting SET transcript_scraped = 0')
    cursor.close()
    con.commit()
