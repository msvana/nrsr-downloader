import click

import tasks.database
import tasks.speeches
import tasks.structure
import tasks.audio


@click.group()
def app():
    pass


app.add_command(tasks.database.initdb)
app.add_command(tasks.structure.structure)
app.add_command(tasks.speeches.speeches)
app.add_command(tasks.audio.videos)
app.add_command(tasks.audio.extract_audio)
app.add_command(tasks.database.export)

if __name__ == '__main__':
    app()
