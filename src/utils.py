import time

import bs4
import requests

import config


def download_and_create_soup(url):
    time.sleep(0.3)
    response = requests.get(url, headers=config.REQUEST_HEADERS)

    if response.status_code != 200:
        raise IOError('Could not download %s' % url)

    soup = bs4.BeautifulSoup(response.text, 'html.parser')
    return soup
