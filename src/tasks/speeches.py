import datetime
import re
import sqlite3

import click

import model.storage
import model.web
import utils


@click.command()
@click.argument('dbpath')
@click.option('--truncate', is_flag=True)
def speeches(dbpath, truncate):
    with sqlite3.connect(dbpath) as con:
        con.row_factory = sqlite3.Row
        if truncate:
            click.echo('Marking all meetings as not scraped ...')
            model.storage.truncate_speeches(con)

        meetings_to_process = model.storage.get_meetings_not_flagged_as('transcript_scraped', con)

        for meeting in meetings_to_process:
            click.echo(f"Processing meeting {meeting['meeting_number']}"
                       f" from period {meeting['election_period']}")
            _process_meeting(meeting, con)
            click.echo(f"Meeting {meeting['meeting_number']}"
                       f" from period {meeting['election_period']} scraped")


def _process_meeting(meeting, con):
    meeting_day_urls = model.web.get_meeting_day_urls(meeting)
    meeting_day_pages = [p for url in meeting_day_urls for p in model.web.get_pages(url)]
    for page in meeting_day_pages:
        click.echo(f'Extracting speeches from {page}')
        page_speeches = _extract_speeches_from_page(page)
        page_speeches_insert = [
            (s['id'], meeting['id'], s['timestamp'], s['speaker'], s['speech_text'],
             s['video_verified'], s['video_start'], s['video_stop'])
            for s in page_speeches]
        cursor = con.cursor()
        cursor.executemany('REPLACE INTO speech VALUES(?,?,?,?,?,?,?,?)', page_speeches_insert)
        cursor.close()
        con.commit()
        click.echo(f"{len(page_speeches)} speeches found and stored in the DB")
    model.storage.flag_meeting_as(meeting, 'transcript_scraped', con)


def _extract_speeches_from_page(page_url):
    soup = utils.download_and_create_soup(page_url)
    speech_tags = soup.find_all('div', 'item')
    page_speeches = []

    for speech_tag in speech_tags:
        if 'day-divider' in speech_tag['class']:
            continue
        speech = _extract_speech(speech_tag)

        if speech['speech_text'] != '':
            page_speeches.append(speech)

    return page_speeches


def _extract_speech(speech_tag):
    span_tags = speech_tag.find_all('span')
    date = span_tags[0].text.strip()
    time = span_tags[1].text.strip()
    datetime_string = '%s %s' % (date, time)
    timestamp = datetime.datetime.strptime(datetime_string, '%d.%m.%Y %H:%M')

    speech = {
        'id': int(speech_tag['data-id']),
        'speaker': speech_tag.find('strong', 'person-title').text.strip(),
        'timestamp': timestamp,
        'video_verified': _is_verified(speech_tag),
        'video_start': int(speech_tag['data-start']),
        'video_stop': int(speech_tag['data-stop']),
        'speech_text': speech_tag.find('div', 'toggle-box').text.strip()
    }

    speech['speech_text'] = re.sub('\s+', ' ', speech['speech_text'])

    return speech


def _is_verified(speech_tag):
    try:
        item_status_tag = speech_tag.find('div', 'item-status')
        item_status_text = item_status_tag.text.strip()
        verified = 1 if item_status_text == 'Skontrolovaný text' else 0
    except AttributeError:
        verified = 0
    return verified
