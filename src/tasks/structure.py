import os
import sqlite3
import urllib.parse

import click

import config
import model.storage
import model.web
import utils


@click.command()
@click.argument('dbpath')
@click.option('--truncate', is_flag=True)
@click.option('--clear-last', is_flag=True)
def structure(dbpath, truncate, clear_last):
    if not os.path.exists(dbpath):
        click.echo(f'Database file `{dbpath}` not found')
        click.echo('You might want to use the `initdb` command')
        return

    click.echo('Extracting election periods URLs ...')
    election_periods = _get_election_periods(config.BASE_URL)
    election_periods_count = len(election_periods)
    click.echo(f'Found {election_periods_count} election periods')

    with sqlite3.connect(dbpath) as con:
        cursor = con.cursor()
        if truncate:
            cursor.execute('DELETE FROM meeting')
            existing_meetings = []
            last_meeting_id = None
        else:
            existing_meetings = model.storage.get_all_meetings(con)
            last_meeting_id = existing_meetings[-1][0]
            existing_meetings = [(m[1], m[2]) for m in existing_meetings]

        for election_period in election_periods:
            click.echo(f'Processing election period no. {election_period[0]}')
            meeting_page_urls = model.web.get_pages(election_period[1])
            for page_url in meeting_page_urls:
                meetings = _get_meeting_urls(page_url)
                for meeting in meetings:
                    meeting_id = election_period[0] * 1000 + meeting[0]
                    if (election_period[0], meeting[0]) in existing_meetings:
                        click.echo(f'Meeting {meeting_id} already exists')
                        continue
                    cursor.execute(
                        '''
                        INSERT INTO meeting (id, election_period, meeting_number, url)
                        VALUES (?, ?, ?, ?)
                        ''',
                        (meeting_id, election_period[0], meeting[0], meeting[1]))
                    click.echo(f'Meeting {meeting_id} inserted into the DB')
            con.commit()

        cursor.execute('SELECT COUNT(*) AS meetings_count FROM meeting')
        meetings_count = cursor.fetchone()[0]
        click.echo(f'{meetings_count} meetings are now stored in the database')

        if clear_last and last_meeting_id:
            click.echo(f'Clearing meeting {last_meeting_id}')
            model.storage.clear_meeting(last_meeting_id, con)

        cursor.close()


def _get_election_periods(base_url):
    soup = utils.download_and_create_soup(base_url)
    election_period_select = soup.find(id='TermNr')

    election_periods = []
    for election_period_option in election_period_select.find_all('option'):
        election_period_number = int(election_period_option['value'])
        url = f'{base_url}/{election_period_number}'
        election_periods.append((election_period_number, url))

    return election_periods


def _get_meeting_urls(period_page_url):
    soup = utils.download_and_create_soup(period_page_url)
    meeting_headings = soup.find_all('h2')

    meeting_urls = []

    for meeting_heading in meeting_headings:
        relative_meeting_url = meeting_heading.find('a')['href']
        meeting_url = urllib.parse.urljoin(period_page_url, relative_meeting_url)
        meeting_number = int(meeting_url.split('/')[-1])
        meeting_urls.append((meeting_number, meeting_url))

    return meeting_urls
