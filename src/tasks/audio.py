import os
import sqlite3
import tempfile

import click
import google.cloud.storage
import moviepy.editor as mp

import config
import model.storage
import model.web
import utils


@click.command()
@click.argument('dbpath')
def videos(dbpath):
    with sqlite3.connect(dbpath) as con:
        con.row_factory = sqlite3.Row
        meetings_to_process = model.storage.get_meetings_not_flagged_as('videos_scraped', con)
        for meeting in meetings_to_process:
            try:
                click.echo(f"Processing meeting {meeting['meeting_number']}"
                           f" from period {meeting['election_period']}")
                _download_meeting_videos(meeting)
                model.storage.flag_meeting_as(meeting, 'videos_scraped', con)
                click.echo(f"Meeting {meeting['meeting_number']}"
                           f" from period {meeting['election_period']} scraped")
            except TypeError:
                click.echo('Video not available.')
                click.echo('Try later, there might be a live session at the moment')


@click.command()
@click.argument('dbpath')
def extract_audio(dbpath):
    with sqlite3.connect(dbpath) as con:
        con.row_factory = sqlite3.Row
        meetings_to_process = model.storage.get_meetings_not_flagged_as('audio_extracted', con)
        for meeting in meetings_to_process:
            click.echo(f"Processing meeting {meeting['meeting_number']}"
                       f" from period {meeting['election_period']}")
            _extract_audio_from_meeting(meeting, con)
            model.storage.flag_meeting_as(meeting, 'audio_extracted', con)
            click.echo(f"Audio for meeting {meeting['meeting_number']}"
                       f" from period {meeting['election_period']} processed")


def _download_meeting_videos(meeting):
    meeting_day_urls = model.web.get_meeting_day_urls(meeting)
    storage_client = google.cloud.storage.Client()
    bucket = storage_client.bucket(config.AUDIO_BUCKET)

    for meeting_day_url in meeting_day_urls:
        soup = utils.download_and_create_soup(meeting_day_url)
        video_url = soup.find('a', 'download-link-btn')['href']
        video_url = f'https://tv.nrsr.sk/{video_url}'
        meeting_date = meeting_day_url.split('=')[1]
        filename = f"{meeting['election_period']}_{meeting['meeting_number']}_{meeting_date}.mp4"
        blob = bucket.blob('videos/%s' % filename)

        if not blob.exists():
            click.echo(f'Downloading video {filename}')
            with tempfile.TemporaryDirectory() as tmp_video_dir:
                filename_tmp = os.path.join(tmp_video_dir, filename)
                os.system('curl -o %s %s' % (filename_tmp, video_url))
                blob.upload_from_filename(filename_tmp)
        else:
            click.echo(f'Video {filename} already downloaded')

    storage_client.close()


def _extract_audio_from_meeting(meeting, con):
    storage_client = google.cloud.storage.Client()
    bucket = storage_client.bucket(config.AUDIO_BUCKET)
    speeches = model.storage.get_speeches_for_meeting(meeting['id'], con)
    prev_base_filename = None
    tmp_file = None
    recording = None

    with tempfile.TemporaryDirectory() as tmp_dir:
        for speech in speeches:
            speech_date = speech['timestamp'].split(' ')[0].split('-')
            base_filename = '%s_%s_%s%s%s.mp4' % (
                meeting['election_period'], meeting['meeting_number'],
                speech_date[2], speech_date[1], speech_date[0])

            if prev_base_filename != base_filename:
                if tmp_file:
                    click.echo('Removing %s' % prev_base_filename)
                    os.system('rm %s' % (tmp_file,))
                click.echo('Downloading %s' % base_filename)
                tmp_file = os.path.join(tmp_dir, base_filename)
                blob = bucket.blob('videos/%s' % base_filename)
                if not blob.exists():
                    click.echo('%s not found' % base_filename)
                    continue
                blob.download_to_filename(tmp_file)
                click.echo('Extracting audio from %s' % base_filename)
                if recording:
                    recording.close()
                recording = mp.VideoFileClip(tmp_file)
                prev_base_filename = base_filename

            audio_output_filename = '%d.m4a' % speech['id']
            audio_output_tmp_file = os.path.join(tmp_dir, audio_output_filename)

            try:
                speech_clip = recording.audio.subclip(speech['video_start'], speech['video_stop'])
                speech_clip.write_audiofile(audio_output_tmp_file, codec='aac')
                audio_blob = bucket.blob(
                    f"audio/{meeting['election_period']}_{meeting['meeting_number']}/"
                    f"{audio_output_filename}")
                audio_blob.upload_from_filename(audio_output_tmp_file)

            except (OSError, ValueError, IndexError) as e:
                click.echo(e)
                click.echo(f"{speech['id']}, {speech['video_start']}, {speech['video_stop']}")

            os.system(f'rm {audio_output_tmp_file}')
    storage_client.close()
