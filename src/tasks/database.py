import csv
import os
import sqlite3

import click

import model.storage


@click.command()
@click.option('--force', is_flag=True)
@click.argument('dbpath')
def initdb(dbpath, force):
    click.echo('Initiating new empty DB ...')

    if os.path.exists(dbpath) and not force:
        return click.echo('The database file already exists. Add --force to override')

    with sqlite3.connect(dbpath) as con:
        cursor = con.cursor()
        cursor.execute('DROP TABLE IF EXISTS meeting')
        cursor.execute(model.storage.Q_CREATE_MEETING_TABLE)
        cursor.execute('DROP TABLE IF EXISTS speech')
        cursor.execute(model.storage.Q_CREATE_SPEECH_TABLE)
        cursor.close()

    click.echo(f'Database initiated in file `{dbpath}`')


@click.command()
@click.argument('dbpath')
@click.argument('outfile')
def export(dbpath, outfile):
    click.echo(f'Exporting to {outfile}')

    with sqlite3.connect(dbpath) as con, open(outfile, 'w') as outfile_fd:
        speeches = model.storage.get_speeches_for_export(con)
        writer = csv.writer(outfile_fd)
        writer.writerow(
            ('id', 'election_period', 'meeting_number', 'timestamp', 'speaker', 'speech_text'))
        writer.writerows(speeches)

    click.echo('Export done')