# NRSR speech extractor

This tool extracts speeches made by members of the Slovak national parliament (NRSR) during
its official meeting sessions. All data is scraped from https://tv.nrsr.sk.

## Requirements and installation

The tool was tested on Python 3.8. Package requirements can be installed through 
[pipenv](https://pypi.org/project/pipenv/) and are listed in the `Pipfile` file.

Assuming you have Python 3 installed you can simply clone this repository:

```bash
$ git clone git@gitlab.com:msvana/nrsr-downloader.git
```

If `pipenv` is not installed on your machine yet, you can do so with `pip`, for example:

```bash
$ pip install pipenv
```

Go to the downloaded project directory and install all dependencies

```bash
$ cd nrsr-downloader/
$ pipenv install
```

Activate the virtual environment created by pipenv and test that you can run the tool

```bash
$ pipenv shell
$ (nrsr-downloader) python src/main.py --help
```

## Usage

The `src/main.py` script provides several subcommands. In the following list they are listed
by the order in which they should be executed

### `initdb [DBPATH]`
This command initialized an empty SQLite database 
which will serve as a storage for all downloaded data (except for video and audio files).
The reason behind using an SQLite database is to remember the current state of scraping.
Since getting all speeches takes a long time, having the ability to continue where 
the scraping stopped when a command ran previously is very important.

### `structure --truncate --clear-last [DBPATH]`
Downloads the basic structure of election periods and meetings.
the optional `--truncate` flag removes all previous meeting information (speeches themselves are preserved though).
The `--clear-last` flag tells the tool to remove all speeches from the most recent meeting and mark it as unscraped.
This is useful when you want to update the database and the most recent meeting wasn't finished at the time of the 
last update.

### `speeches --truncate [DBPATH]`
Downloads speeches for each meeting that wasn't scraped yet and stores them in the database. The optional `--truncate`
flag makes the command to redownload speeches even for meetings already marked as processed.

### `videos [DBPATH]`
Downloads video recordings for all meeting. These are currently stored in a Google Cloud Storage bucket. Bucket name
can be set in the `src/config.py` file. Videos that were previously downloaded are not downloaded
again unless the corresponding video file is deleted from the bucket.

### `extract-audio [DBPATH]`
Attempts to extract audio corresponding to each individual speech from previously downloaded
video files. Each speech transcript on https://tv.nrsr.sk/ contains a set of start and stop
timestamps that should identify a video clip of a given speech. Unfortunatelly, these
timestamps are often incorrect. In the future we plan to look into using advanced techniques
to find the speech in the video.

Extracted audio is again stored in the Google Cloud Storage Bucket

### `export [DBPATH] [OUTFILE]`
Exports the speeches as a CSV file. This file contains following columns: 
`id`, `election_period`, `meeting_number`, `timestamp`, `speaker`, and `speech_text`. 

## Example sequence of commands

```bash
$ python src/main.py initdb /tmp/db.sqlite
$ python src/main.py structure /tmp/db.sqlite
$ python src/main.py speeches /tmp/db.sqlite
$ python src/main.py videos /tmp/db.sqlite
$ python src/main.py extract-audio /tmp/db.sqlite
$ python src/main.py export /tmp/db.sqlite /tmp/nrsr.csv
```